var express = require('express');
var fs = require('fs');
var multer = require('multer');
var upload = multer({dest: "/path/to/temporary/directory/to/store/uploaded/files"});
var path = require('path');

var app = express();

var obj = [];
var i;

//---Отдельные статьи---

module.exports=async function(app){
    app.get('/article/:id', function(req, res) {
        var amount = JSON.parse(fs.readFileSync('./public/material/amount.json', 'utf8'));
        var n = amount.amount;
        for (i = 1; i <= n; i++) {
            obj[i] = JSON.parse(fs.readFileSync('./public/material/' + i + '.json'));
        }
        res.render('article', {obj: obj[req.params.id], id: req.params.id});
    });
 }