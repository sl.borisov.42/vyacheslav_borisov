var express = require('express');
var fs = require('fs');

var app = express();

var amount = JSON.parse(fs.readFileSync('./public/material/amount.json', 'utf8'));
var n = amount.amount;

//---Начало исполняемой части---

var port = 3000; //Отслеживаемый порт

app.set('view engine', 'ejs');
app.use('/public', express.static('public'));

require('./pg_index-lng')(app); //index через "/index" --- Главная
require('./pg_index-shrt')(app); //index через "/" --- Главная
require('./pg_article')(app); //article/:id --- Отдельные статьи
require('./pg_add-gt')(app); //add --- Форма добавления статьи
require('./pg_add-pst')(app); //add --- Обработчик для формы добавления статей
require('./pg_del-gt')(app); //del --- Удаление статей
require('./pg_del-pst')(app); //del --- Обработчик для удаления статей
require('./pg_404')(app); //del --- Обработчик для удаления статей

app.listen(port);
console.log('Сервер запущён, порт: ' + port + ', количество статей на момент запуска: ' + n);
