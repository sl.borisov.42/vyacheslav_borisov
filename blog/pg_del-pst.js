var express = require('express');
var fs = require('fs');
var multer = require('multer');
var upload = multer({dest: "/path/to/temporary/directory/to/store/uploaded/files"});
var path = require('path');

var app = express();

var obj = [];
var i;

//---Обработчик для удаления статей---

module.exports=async function(app){
    app.get('/del/:id', function(req, res) {
        var amount = JSON.parse(fs.readFileSync('./public/material/amount.json', 'utf8'));
        var n = amount.amount;
        for (i = 1; i <= n; i++) {
            obj[i] = JSON.parse(fs.readFileSync('./public/material/' + i + '.json'));
        }
        var d = Number.parseInt(req.params.id);
        var title = obj[d].title;
        for (i = d; i < n; i++) {
            i = Number.parseInt(i);
            var next;
            next = Number.parseInt(next);
            next = i+1;
            fs.renameSync('./public/material/' + next + '.json','./public/material/' + i + '.json');
            fs.renameSync('./public/uploads/img' + next + '.png','./public/uploads/img' + i + '.png');
        };
        fs.unlink('./public/material/' + n + '.json', function() {});
        fs.unlink('./public/uploads/img' + n + '.png', function() {});
        n = n - 1;
        amount.amount = n;
        var callback;
        var jsonamount = JSON.stringify(amount);
        fs.writeFile('./public/material/amount.json', jsonamount, 'utf8', callback);
        res.render('del-succes', {title: title});
    });
 }