var express = require('express');
var fs = require('fs');
var multer = require('multer');
var upload = multer({dest: "/path/to/temporary/directory/to/store/uploaded/files"});
var path = require('path');

var app = express();

var obj = [];
var i;

//---Обработчик для формы добавления статей---

module.exports=async function(app){
    app.post( "/add", upload.single("upload"), 
        (req, res) => {
            var amount = JSON.parse(fs.readFileSync('./public/material/amount.json', 'utf8'));
            var n = amount.amount;
            console.log(req.file);
            n++;
            if (req.file != undefined) {
                const tempPath = req.file.path;
                const targetPath = path.join(__dirname, "./public/uploads/img" + n + ".png");
            
                if (path.extname(req.file.originalname).toLowerCase() === ".png" || path.extname(req.file.originalname).toLowerCase() === ".jpg") {
                    fs.rename(tempPath, targetPath, err => {
                        if (err) return handleError(err, res);
                        res
                            .status(200)
                            .contentType("text/plain")
                            .end('Successfully! Go back to the last page');
                        });
                } else {
                fs.unlink(tempPath, err => {
                if (err) return handleError(err, res);
                res
                    .status(403)
                    .contentType("text/plain")
                    .end('ERROR: Only .jpg or .png! Go back to the last page');
                });
                }
            };  
            amount.amount = n;
            var newreq = req.body;
            console.log(newreq, amount);
            var callback;
            if (req.file == undefined) {
                newreq.img = 0;
                fs.writeFileSync("./public/uploads/img" + n + ".png")
            } else {
                newreq.img = 1;
            }
            var jsonamount = JSON.stringify(amount);
            var jsonnewreq = JSON.stringify(newreq);
            fs.writeFile('./public/material/amount.json', jsonamount, 'utf8', callback);
            fs.writeFile('./public/material/' + n + '.json', jsonnewreq, 'utf8', callback);
            if (req.file == undefined) res.end('Successfully! Go back to the last page');
        }
    );
 }