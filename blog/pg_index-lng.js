var express = require('express');
var fs = require('fs');
var multer = require('multer');
var upload = multer({dest: "/path/to/temporary/directory/to/store/uploaded/files"});
var path = require('path');

var app = express();

var obj = [];
var i;

//---Главная---

module.exports=async function(app){
    app.get('/index', function(req, res) {
        var amount = JSON.parse(fs.readFileSync('./public/material/amount.json', 'utf8'));
        var n = amount.amount;
        for (i = 1; i <= n; i++) {
            obj[i] = JSON.parse(fs.readFileSync('./public/material/' + i + '.json'))
        }
        res.render('index', {obj: obj, n: n});
    });
 }